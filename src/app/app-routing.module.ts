import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarteComponent } from './carte/carte.component';
import { EnregistrerCommandeComponent } from './enregistrer-commande/enregistrer-commande.component';
import { AfficherCommandesComponent } from './afficher-commandes/afficher-commandes.component';
import { GererFlotteComponent } from './gerer-flotte/gerer-flotte.component';
import { AnomalieComponent } from './anomalie/anomalie.component';


const routes: Routes = [
  { path: '', component: CarteComponent },
  { path: 'enregistrer-commande', component: EnregistrerCommandeComponent },
  { path: 'afficher-commandes', component: AfficherCommandesComponent },
  { path: 'gerer-flotte', component: GererFlotteComponent },
  { path: 'anomalie', component: AnomalieComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
