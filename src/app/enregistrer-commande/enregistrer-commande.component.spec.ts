import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnregistrerCommandeComponent } from './enregistrer-commande.component';

describe('EnregistrerCommandeComponent', () => {
  let component: EnregistrerCommandeComponent;
  let fixture: ComponentFixture<EnregistrerCommandeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnregistrerCommandeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnregistrerCommandeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
