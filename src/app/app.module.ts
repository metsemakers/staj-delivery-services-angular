import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './shared/components/menu/menu.component';
import { CarteComponent } from './carte/carte.component';

import { from } from 'rxjs';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

import { EnregistrerCommandeComponent } from './enregistrer-commande/enregistrer-commande.component';
import { AfficherCommandesComponent } from './afficher-commandes/afficher-commandes.component';
import { GererFlotteComponent } from './gerer-flotte/gerer-flotte.component';
import { AnomalieComponent } from './anomalie/anomalie.component';
import { DroneWebService } from './shared/web-services/drone.webservice';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    CarteComponent,
    EnregistrerCommandeComponent,
    AfficherCommandesComponent,
    GererFlotteComponent,
    AnomalieComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    LeafletModule.forRoot(),
    HttpClientModule
  ],
  providers: [
    DroneWebService
  ],
  bootstrap: [AppComponent]

})
export class AppModule { }
