import { Component, OnInit, OnDestroy } from '@angular/core';
import * as L from 'leaflet';
import { DroneWebService } from '../shared/web-services/drone.webservice';
import { Subscription } from 'rxjs';
import { Drone } from '../shared/models/drone.model';

@Component({
  selector: 'app-carte',
  templateUrl: './carte.component.html',
  styleUrls: ['./carte.component.scss']
})
export class CarteComponent implements OnInit, OnDestroy {

  droneToFind: number;
  lat = 44.8333;
  lon = -0.5667;

  private subscriptionMessage$: Subscription;

  dronesList: Drone[] = [];
  isComplete = false;

  constructor(private droneWebService: DroneWebService) { }

  ngOnInit() {
    this.subscriptionMessage$ = this.droneWebService.getDrones().subscribe(
      (data) => {
        // Next
        console.log('CallObservableComponent Next', data);
        this.dronesList = data;

        const myMap = L.map('map').setView([this.lat, this.lon], 5);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            minZoom: 1,
            maxZoom: 20
        }).addTo(myMap);
        const myIcon = L.icon({
          iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/images/marker-icon.png'
        });
        console.log('droneList length ' + this.dronesList.length);
        this.dronesList.forEach(element => {
          console.log('element - ' + element.latitude);
          L.marker([element.longitude, element.latitude], {icon: myIcon}).addTo(myMap);
        });

      }, (error) => {
        // Error
        console.error('CallObservableComponent error', error);
      }
    );
  }

  ngOnDestroy() {
    console.log('displaySelectComponent destroy! Boom !');
    if (this.subscriptionMessage$) {
      console.log('displaySelectComponent unsubscribe!');
      this.subscriptionMessage$.unsubscribe();
    }
  }
}
